/*
 * Main.cpp
 *
 *  Created on: Fall 2019
 */

#define cimg_display  1

#include <stdio.h>
#include <math.h>
#include <CImg.h>
#include <immintrin.h>

using namespace cimg_library;

//Tipo de dato para los componentes de la imagen. 
//Para nuestro grupo: float
typedef float data_t;
const int ITEMS_PER_PACKET(sizeof(__m256) / sizeof(data_t));

const char* SOURCE_IMG = "bailarina.bmp";
const char* DESTINATION_IMG = "bailarina2.bmp";
const char* FILTER_IMG = "background_V.bmp";

const int NUMBER_OF_ITERATIONS = 40;

int main() {

	// Open file and object initialization
	CImg<data_t> srcImage(SOURCE_IMG);

	data_t* pRsrc, * pGsrc, * pBsrc; // Pointers to the R, G and B components
	data_t* pRdest, * pGdest, * pBdest;
	data_t* pDstImage; // Pointer to the new image pixels

	uint width, height, width2, height2; // Width and height of the images
	uint nComp; // Number of image components

	/***************************************************
	 *  Variables initialization.
	 *   - Prepare variables for the algorithm
	 *   - This is not included in the benchmark time
	 */
	CImg<data_t> srcImageY(FILTER_IMG);

	data_t* pRsrcY, * pGsrcY, * pBsrcY;

	__m256 vRsrcY, vGsrcY, vBsrcY, vRsrc, vGsrc, vBsrc, v255, v256, v1, vTemp;

	struct timespec tStart, tEnd;
	double dElapsedTime;

	srcImage.display(); // Displays the source image
	srcImageY.display();
	width = srcImage.width(); // Getting information from the source image
	height = srcImage.height();
	nComp = srcImage.spectrum(); // source image number of components
				// Common values for spectrum (number of image components):
				//  B&W images = 1
				//	Normal color images = 3 (RGB)
				//  Special color images = 4 (RGB and alpha/transparency channel)

	width2 = srcImageY.width();		// Getting information from the filter image
	height2 = srcImageY.height();

	if (width != width2 || height != height2) {	//checks if the two images have the same dimensions
		perror("El tamaño de la imagen a modificar y la imagen filtro no coincide");
		exit(EXIT_FAILURE);
	}

	//e

	int vector_size = width * height; // Array size. Note: It is not a multiple of 8
	int nPackets = (vector_size * sizeof(float) / sizeof(__m256));

	// If it is not a exact number we need to add one more packet
	if (((vector_size * sizeof(float)) % sizeof(__m256)) != 0) {
		nPackets++;
	}

	//// Data arrays to sum. May be unaligned to __m256 size (32 bytes)
	data_t a1[ITEMS_PER_PACKET], a255[ITEMS_PER_PACKET], a256[ITEMS_PER_PACKET];

	for (int i = 0; i < ITEMS_PER_PACKET; i++) {
		*(a1 + i) = (data_t)(1);       
		*(a255 + i) = (data_t)(255);       
		*(a256 + i) = (data_t)(256);
	}

	// Create an array aligned to 32 bytes (256 bits) to store the sum
	// Aligned memory access improves performance
	data_t* temp = (float*)_mm_malloc(sizeof(__m256) * ITEMS_PER_PACKET, sizeof(__m256));

	// Allocate memory space for destination image components
	pDstImage = (data_t*)_mm_malloc(sizeof(__m256) * height * width * nComp, sizeof(__m256));
	if (pDstImage == NULL) {
		perror("Allocating destination image");
		exit(-2);
	}

	////inicializar todos los elementos a -1.
	//for (int i = 0; i < nPackets * nComp; i++) {
	//	*(__m256*)(temp + i * ITEMS_PER_PACKET) = _mm256_set1_ps(0);
	//	*(__m256*)(res + i * ITEMS_PER_PACKET) = _mm256_set1_ps(0);
	//}

	// Pointers to the component arrays of the source image
	pRsrc = srcImage.data(); // pRcomp points to the R component array
	pGsrc = pRsrc + height * width; // pGcomp points to the G component array
	pBsrc = pGsrc + height * width; // pBcomp points to B component array

	// Pointers to the componet arrays of the second source image
	pRsrcY = srcImageY.data(); // pRcomp points to the R component array
	pGsrcY = pRsrcY + height * width; // pGcomp points to the G component array
	pBsrcY = pGsrcY + height * width; // pBcomp points to B component array

	// Pointers to the RGB arrays of the destination image
	pRdest = pDstImage;
	pGdest = pRdest + height * width;
	pBdest = pGdest + height * width;

	/***********************************************
	 *  Inicio del algoritmo
	 *   - Medir tiempo inicial
	 */
	if (clock_gettime(CLOCK_REALTIME, &tStart) == -1)
	{
		perror("clock_gettime");
		exit(EXIT_FAILURE);
	}

	v1 = _mm256_loadu_ps(a1);
	v255 = _mm256_loadu_ps(a255);
	v256 = _mm256_loadu_ps(a256);

	for (int i = 0; i < NUMBER_OF_ITERATIONS; i++) {
		for (uint i = 0; i < nPackets; i++) {

			vRsrcY = _mm256_loadu_ps(pRsrcY + i * ITEMS_PER_PACKET);
			vRsrc = _mm256_loadu_ps(pRsrc + i * ITEMS_PER_PACKET);
			*(__m256*)(pRdest + i * ITEMS_PER_PACKET) = _mm256_sub_ps(v255, _mm256_div_ps(_mm256_mul_ps(v256, _mm256_sub_ps(v255, vRsrcY)), _mm256_add_ps(vRsrc, v1)));

			vGsrcY = _mm256_loadu_ps(pGsrcY + i * ITEMS_PER_PACKET);
			vGsrc = _mm256_loadu_ps(pGsrc + i * ITEMS_PER_PACKET);
			*(__m256*)(pGdest + i * ITEMS_PER_PACKET) = _mm256_sub_ps(v255, _mm256_div_ps(_mm256_mul_ps(v256, _mm256_sub_ps(v255, vGsrcY)), _mm256_add_ps(vGsrc, v1)));

			vBsrcY = _mm256_loadu_ps(pBsrcY + i * ITEMS_PER_PACKET);
			vBsrc = _mm256_loadu_ps(pBsrc + i * ITEMS_PER_PACKET);
			*(__m256*)(pBdest + i * ITEMS_PER_PACKET) = _mm256_sub_ps(v255, _mm256_div_ps(_mm256_mul_ps(v256, _mm256_sub_ps(v255, vBsrcY)), _mm256_add_ps(vBsrc, v1)));

		}
	}

	/***********************************************
	 * End of the algorithm.
	 *   - Measure the end time
	 *   - Calculate the elapsed time
	 */
	if (clock_gettime(CLOCK_REALTIME, &tEnd) == -1)
	{
		perror("clock_gettime");
		exit(EXIT_FAILURE);
	}

	dElapsedTime = (tEnd.tv_sec - tStart.tv_sec);
	dElapsedTime += (tEnd.tv_nsec - tStart.tv_nsec) / 1e+9;
	printf("Tiempo de ejecución: %f s.\n", dElapsedTime);

	// Create a new image object with the calculated pixels
	// In case of normal color images use nComp=3,
	// In case of B/W images use nComp=1.
	CImg<data_t> dstImage(pDstImage, width, height, 1, nComp);

	// Store destination image in disk
	dstImage.save(DESTINATION_IMG);

	// Display destination image
	dstImage.display();

	// Free memory
	_mm_free(pDstImage);
	_mm_free(temp);

	return 0;
}

